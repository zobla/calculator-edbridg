import React from 'react';
import './App.css';
import { Container, Row, Col } from 'reactstrap';
import FormComponent from './component/Form/form.component';
import GraphComponent from './component/Graph/graph.component';
import Calculate from './calculate';

const initialState = () => ({
  formData: {},
  output: {},
  plots: [],
  rateOfInterest: 0,
  repaymentProportionRate: 0
})
class App extends React.Component {

  constructor(props) {
    super(props)
     this.state = initialState()
  }

  render() {
    return <Container className='mt-4 mb-4'>
      <Row>
        <Col md={12}>
          <h2>Calculator Application</h2>
        </Col>
        <Col>
          <FormComponent
            calculate={this.calculate.bind(this)}
            rateOfInterest={this.state.rateOfInterest}
            repaymentProportionRate={this.state.repaymentProportionRate}
          />
        </Col>
        <Col md={12}>
          <GraphComponent data={this.state.bankPlot}/>
        </Col>
      </Row>
    </Container>
  }

  calculate(state) {
    let { maturity, tuition, program, moneyMultiple } = state;
    let processor = new Calculate;
    let {bankPlot, rateOfInterest, repaymentProportionRate} = processor.init({
      maturity,
      tuition,
      program,
      moneyMultiple
    });

    this.setState({
      bankPlot,
      rateOfInterest,
      repaymentProportionRate
    });

  }
}

export default App;
