import constants from './constants'

class Calculate {

    constructor() {
        this.data = {
            startingSalary: constants.STARTING_SALARY,
            growthRate: constants.SALARY_GROWTH_RATE,
            interest: constants.BANK_INTEREST_RATE
        }
    }

    init = (initData) => {
        this.data = {
            ...this.data,
            ...initData
        };

        let { bankPlot } = this.forBank();
        let { payments, rateOfInterest, repaymentProportionRate } = this.forEdbridg();

        return {
            bankPlot: bankPlot.map((bankPlot, idx) => {
                return {
                    ...bankPlot,
                    ...(payments[idx] ? payments[idx] : {}),
                }
            }),
            rateOfInterest,
            repaymentProportionRate
        }

    }

    forBank = () => {
        let { maturity, tuition, interest } = this.data;

        let discount = this.calculateDiscount({
            interest,
            maturity
        })

        let annualPayment = Number(tuition) / discount,
            bankPlot = [];

        for (let i = 0; i < maturity; i++) {
            bankPlot.push({
                "Year": new Date().getFullYear() + i,
                "Bank": Math.round(annualPayment),
                "BankColor": "hsl(350, 70%, 50%)",
            })
        }

        return {
            bankPlot,
            discount,
            annualPayment
        }
    }

    forEdbridg = () => {
        let { maturity, tuition, moneyMultiple, startingSalary, growthRate, interest } = this.data,
        payments = [], salaries = [Number(startingSalary)];

        let amount = 0, totalAmount = 0;

        for (let i = 0; i < maturity; i++) {
            if(!i) {
                amount = Number(interest / 100) * startingSalary;
            } else {
                amount = Number(interest / 100) * salaries[i-1] * (Number(growthRate / 100) + 1);
                salaries.push(Number(salaries[i-1]) * (Number(growthRate / 100) + 1))
            }
            
            payments.push({
                EDBRIDG: amount.toFixed(2),
                EDBRIDGColor: "hsl(250, 70%, 50%)"
            });

            totalAmount = Number(amount) + Number(totalAmount);
        }

        let repaymentProportionRate = (Number(moneyMultiple)*Number(tuition))/(Number(salaries.reduce((a, b) => a + b, 0)))
        
        let rateOfInterest = Math.pow(
            (Number(repaymentProportionRate) * Number(salaries.reduce((a, b) => a + b, 0)) / Number(tuition)), 
            1/Number(maturity)
        ) - 1;
    
        return {
            payments,
            totalAmount,
            repaymentProportionRate,
            rateOfInterest
        }
    }

    calculateDiscount = ({
        interest,
        maturity
    }) => {
        interest = Number(interest) / 100;
        let bits = Math.pow(Number(interest) + 1, Number(maturity));
        return (bits - 1) / (interest * bits)
    }

}

export default Calculate;