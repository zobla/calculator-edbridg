import React from 'react';
import { ResponsiveBar } from '@nivo/bar';
import {Button} from 'reactstrap';
import html2canvas from 'html2canvas';
const pdfConverter = require('jspdf');


const data = []

const demoFromHTML = () => {
  let input = window.document.getElementsByClassName("divToPDF")[0];
  html2canvas(input).then(canvas => {
    const imgData = canvas.toDataURL("image/png");
    const pdf = new pdfConverter("l", "pt");
    pdf.addImage(imgData, "JPEG", 10, 110, 800, 250);
    pdf.save("test.pdf");
  });
};

const GraphComponent = function ({ data = [] }) {
    console.log("TCL: GraphComponent -> data", data)
    return <div className="divToPDF" style={{height: 500}}>
        <ResponsiveBar
            data={data}
            keys={['EDBRIDG', 'Bank']}
            indexBy="Year"
            margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
            padding={0.2}
            groupMode="grouped"
            colors={{ scheme: 'nivo' }}
            defs={[
                {
                    id: 'dots',
                    type: 'patternDots',
                    background: 'inherit',
                    color: '#38bcb2',
                    size: 4,
                    padding: 1,
                    stagger: true
                },
                {
                    id: 'lines',
                    type: 'patternLines',
                    background: 'inherit',
                    color: '#eed312',
                    rotation: -45,
                    lineWidth: 6,
                    spacing: 10
                }
            ]}
            fill={[
                {
                    match: {
                        id: 'fries'
                    },
                    id: 'dots'
                },
                {
                    match: {
                        id: 'sandwich'
                    },
                    id: 'lines'
                }
            ]}
            borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
            axisTop={null}
            axisRight={null}
            axisBottom={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'Year',
                legendPosition: 'middle',
                legendOffset: 32
            }}
            axisLeft={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'Amount',
                legendPosition: 'middle',
                legendOffset: -40
            }}
            labelSkipWidth={12}
            labelSkipHeight={12}
            labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
            legends={[
                {
                    dataFrom: 'keys',
                    anchor: 'bottom-right',
                    direction: 'column',
                    justify: false,
                    translateX: 120,
                    translateY: 0,
                    itemsSpacing: 2,
                    itemWidth: 100,
                    itemHeight: 20,
                    itemDirection: 'left-to-right',
                    itemOpacity: 0.85,
                    symbolSize: 20,
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemOpacity: 1
                            }
                        }
                    ]
                }
            ]}
            animate={true}
            motionStiffness={90}
            motionDamping={15}
        />
        <Button onClick={() => demoFromHTML()}>Download PDF</Button>
    </div>

}

export default GraphComponent;