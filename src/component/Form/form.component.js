import React, { Component } from 'react';
import { FormGroup, Label, Col, Input, Row, Button } from 'reactstrap';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const initialState = () => ({
    program: '',
    tuition: 5000,
    maturity: 2,
    moneyMultiple: '',
    maturityMarks: {
        min: 2,
        max: 10
    }
});

const programs = [{
    label: 'MBA Program',
    value: 1
}, {
    label: 'Coding School',
    value: 2
}];

const moneyMultiples = () => {

    let multiples = [];

    for (let i = 0.1; i <= 2.1; i = i + 0.1) {
        multiples.push({
            label: Number(i).toFixed(1) + 'x',
            value: Number(i).toFixed(1)
        });
    }

    return multiples;
}

const tuitionMarks = () => {
    let marks = [];

    for (let i = 5000; i < 50000; i = i + 2000) {
        marks[i] = i;
    }

    return marks;
}

class FormComponent extends Component {

    constructor(props) {
        super(props);

        this.state = initialState();
        this.handleChange = this.handleChange.bind(this);
        this.maturityMarks = this.maturityMarks.bind(this);
    }

    maturityMarks() {
        let marks = [], factor = this.state.program === '2' ? 4 : 10;

        for (let i = 2; i < factor; i++) {
            marks[i] = i;
        }

        return marks;
    }

    render() {
        const { maturityMarks, maturity, tuition } = this.state;

        return <Row>
            <Col>
                <FormGroup key={1}>
                    <Label for="exampleSelect" sm={2}>Program</Label>

                    <Input type="select" name="select" onChange={e => this.handleChange(e, 'program')} invalid={!this.state.program}>
                        <option value=''>Select a value</option>
                        {programs.map(program => <option value={program.value}>{program.label}</option>)}
                    </Input>
                </FormGroup>
                <FormGroup>
                    <Label>Tuition</Label>
                    <Row>
                        <Col md={10}>
                            <Slider
                                defaultValue={5000}
                                value={tuition}
                                min={5000}
                                max={50000}
                                step={100}
                                marks={tuitionMarks()}
                                onChange={e => this.handleChange(e, 'tuition')}
                            />
                        </Col>
                        <Col>
                            <Input type="text" value={tuition} onChange={e => this.handleChange(e.target.value, 'tuition')} />
                        </Col>
                    </Row>
                </FormGroup>
                <FormGroup key={2}>
                    <Label>Maturity</Label>
                    <Row>
                        <Col md={10}>
                            <Slider
                                defaultValue={2}
                                value={maturity}
                                min={maturityMarks.min}
                                max={maturityMarks.max}
                                step={1}
                                marks={this.maturityMarks()}
                                onChange={e => this.handleChange(e, 'maturity')}
                            />
                        </Col>
                        <Col>
                            <Input type="text" value={maturity} onChange={e => this.handleChange(e.target.value, 'maturity')} />
                        </Col>
                    </Row>
                </FormGroup>
                <FormGroup key={4}>
                    <Label for="exampleSelect" sm={2}>Money Multiple</Label>
                    <Input type="select" name="select" onChange={e => this.handleChange(e, 'moneyMultiple')} invalid={!this.state.moneyMultiple}>
                        <option value=''>Select a value</option>
                        {moneyMultiples().map(multiple => <option value={multiple.value}>{multiple.label}</option>)}
                    </Input>
                </FormGroup>
                <Button color="primary" onClick={e => this.props.calculate(this.state)} disabled={this.validate()}>Calculate</Button>
            </Col>

            <Col md={12}>
                <h3>Outputs</h3>
                <FormGroup>
                    <Label for="exampleSelect" sm={2}>Percantages of salary</Label>
                    <Col sm={3}>
                        <div className="form-control">{Number(this.props.repaymentProportionRate*100).toFixed(2)}</div>
                    </Col>
                </FormGroup>
            </Col>
        </Row>
    }

    handleChange(e, type) {
        switch (true) {
            case type === 'tuition' || type === 'maturity':
                this.setState(prevState => {
                    prevState[type] = e;
                    prevState.maturityMarks.max = this.state.program === '2' ? 4 : 10;
                    return prevState;
                });
                break;

            default:
                let { target } = e;
                this.setState(prevState => {
                    prevState[type] = target.value;
                    return prevState;
                });
        }
    }

    validate() {
        let { program, tuition, maturity, moneyMultiple } = this.state;

        if (!program || !tuition || !maturity || !moneyMultiple) {
            return true;
        }

        return false;
    }
}

export default FormComponent;